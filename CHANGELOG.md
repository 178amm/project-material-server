# Project Material Server
Server app & dashboard for Project Material using Parse.

## [Unreleased]

### Added
- Basic configuration

## [0.0.0] - 2016-05-24
### Added
- Project initialization

[0.0.0]: https://bitbucket.org/dadrimon2-admin/postsingson-cms/branches/compare/v0.1%0Ddevelop#commits